package com.msf.webviewsample;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class WebViewActivity extends AppCompatActivity {
    private WebView sampleWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        sampleWebView = findViewById(R.id.wvSample);
        final ProgressBar progressBar = findViewById(R.id.pbWebView);
        loadWeb();
        sampleWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void loadWeb() {
        String url = "https://www.youtube.com";
        sampleWebView.getSettings().setJavaScriptEnabled(true);
        sampleWebView.loadUrl(url);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && this.sampleWebView.canGoBack()) {
            this.sampleWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
